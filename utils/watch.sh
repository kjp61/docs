#!/bin/bash
# Watch for modifications and rebuild the dev docs automatically.
# Depends on inotify-tools (apt install inotify-tools)

cd $(dirname $0)
cd ..

inotifywait -qmr -e modify --exclude '\.?#.*' modules/ \
  | while read event; do
      echo $(date +'%F %T') $event
      npx antora build-dev.yml
    done
