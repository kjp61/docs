= README

Documenting with Antora, AsciiDoc and GitLab pages:  
https://peertube.debian.social/w/fYZmuKtNDabeGJs7fQiX6d

== Installation

. https://github.com/nvm-sh/nvm#installation-and-update[Install `nvm`]:
+
[source,bash]
....
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
source ~/.bashrc
command -v nvm
....

. Install the latest Node LTS release:
+
[source,bash]
....
nvm install --lts
node --version
....

. Install antora packages:
+
[source,bash]
....
git clone https://gitlab.com/lugbz/docs
cd docs/

npm install
npx antora -v
....

== Build the docs

- For development: +
  `npx antora build-dev.yml`

- For the online site: +
  `CI=true npx antora build.yml`
